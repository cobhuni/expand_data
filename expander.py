#!/usr/bin/env python
#
#    expander.py
#
# Copyright 2017 Alicia González Martínez
# COBHUNI Project, Universität Hamburg
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# dependencies:
#   * kabkaj v1.0 #FIXME
#   * FarasaSegmenterJar.jar
#   * FarasaPOSJar.jar
#     lib/
#
# usage:
#   $ python expander.py --all
#   $ python expander.py --ocred --farasa 2>&1 | tee xxx.log
#
########################################################################

import os
import io
import re
import sys
import tempfile
import subprocess
import ujson as json
from pprint import pprint #DEBUG

from argparse import ArgumentParser
from collections import namedtuple
from itertools import groupby

import util
import getdata
import getmetadata

try:
    import kabkaj
except ModuleNotFoundError as err:
    print('{RED}Dependency Error: {err}{RESET}'.format(**globals()), file=sys.stderr)
    sys.exit(1)

RED   = '\033[1;31m'
RESET = '\033[0;0m'

DIACRITICS = 'ًٌٍَُِّْ'

REMOVE_DIACRITICS = re.compile('[%s]' % DIACRITICS)

QURAN_REF_REGEX = re.compile(r'(#.+?#)')
HADITH_REF_REGEX = re.compile(r'(§.+?§)')
SPACES_REGEX = re.compile(r'^\s+$')

MYPATH    = os.path.dirname(os.path.realpath(__file__))
TAGGER    = os.path.join(MYPATH, 'FarasaPOSJar.jar')

Token = namedtuple('Token', ['tok', 'stem', 'var', 'tag', 'ini', 'end'])

def tokenise_adjust(cfg, doc, fname, farasa, debug):
    """  tokenise text and adjust rest of annotation offsets

    Args:
        cfg (ConfigParser): configuration data.
        doc (CorpusDoc): data from the document.
        fname (str): file name being processed. Used for debbuging.
        farasa (bool): use farasa tagger.
        debug (book): print debugging info from tokeniser to stderr.

    Returns:
        dict: newdata for document

    """
    # adjust quranic and hadith references
    for regex, label in ((QURAN_REF_REGEX, 'quran'), (HADITH_REF_REGEX, 'hadith')):

        ref = []
        for m in list(regex.finditer(doc.text))[::-1]:

            doc.text = doc.text[            : m.start()] + \
                       doc.text[m.start()+1 : m.end()-1] + \
                       doc.text[m.end()     :          ]

            for tag, annotations in doc.annotation.items():
                for i in range(len(annotations)):
                    
                    if annotations[i]['ini'] >= m.end(): annotations[i]['ini']   -= 1
                    if annotations[i]['ini'] >= m.start(): annotations[i]['ini'] -= 1
    
                    if annotations[i]['end'] >= m.end(): annotations[i]['end']   -= 1
                    if annotations[i]['end'] >= m.start(): annotations[i]['end'] -= 1
            
            for i in range(len(ref)):
                ref[i][0]-=2
                ref[i][1]-=2

            ref.append([m.start(), m.end()-2])

        # shift first ann in ref to first non-space char
        if ref:
            while SPACES_REGEX.match(doc.text[:ref[-1][0]]):
                ref[-1][0] += 1
    
        doc.annotation[label] = [{cfg.get('annotation', 'ini key') : ini,
                                  cfg.get('annotation', 'end key') : end,
                                  cfg.get('annotation', 'value key') : label} for ini, end in ref[::-1]]


    # add newline at end of text if it does not already end in it
    source_text = doc.text if doc.text.endswith('\n') else '%s\n' % doc.text

    if not farasa:
        text_stream = io.StringIO(source_text)
        tokens = list(kabkaj.tokeniser.tokenise(text_stream, skeleton_copy=True))

    #TODO add third possibility: using farasa and its result overriding the source text; for altafir and hadithalislam ???
    else:

        tokens = []

        source_fd, source_path = tempfile.mkstemp()
        tagged_fd, tagged_path = tempfile.mkstemp()
    
        try:
            with os.fdopen(source_fd, 'w') as source_fp:
                source_fp.write(source_text)
    
            subprocess.call(['java', '-jar', TAGGER, '-i', source_path, '-o', tagged_path])
    
            with os.fdopen(tagged_fd) as tagged_fp:
        
                result = (list(g) for k, g in groupby(tagged_fp.read().split(), lambda s: s == 'S/S') if not k)
                tokens_info = ' '.join(tk for st in (s[:-1] for s in result if s[-1]=='E/E') for tk in st)

                tokens_info = re.sub(r'ال\+/DET (.+?/.+?)(?= |$)', r'ال≥+\1', tokens_info)
                tokens_info = re.sub(r'( [^ ]+)/([^ ]+) \+([^/]+?)/CASE', r'\1+≤\3/\2', tokens_info)
                tokens_info = re.sub(r'( [^ ]+)/([^ ]+) \+([^/]+?)/NSUFF', r'\1+\3/\2', tokens_info)
                tokens_info = re.sub(r'( [^ ]+)/([^ ]+) \+([^/]+?)/NSUFF', r'\1+\3/\2', tokens_info) #NOTE there's the example هجر/NOUN-FS +ت/NSUFF +ين/NSUFF
                tokens_info = re.sub(r'ل\+/PREP ال≥\+(.+?)/', r'ل+/PREP ل≥+\1/', tokens_info)
    
                #if debug:
                #    print('@DEBUG::tokens_info@', file=sys.stderr) #DEBUG
                #    print(tokens_info, file=sys.stderr) #DEBUG
                #    sys.exit() #DEBUG

                if debug: print('\n@DEBUG::-@ source_text=%s' % repr(source_text), file=sys.stderr) #DEBUG
                if debug: print('\n@DEBUG::-@ tokens_info=%s' % repr(tokens_info), file=sys.stderr) #DEBUG
                
                i = 0
                while source_text[i] in (' ', '\n', '\t'):
                    i += 1
                    
                for tok, tag in (token.split('/') for token in tokens_info.split()):

                    tok_ = ''.join(filter(lambda x: x not in ('+', '≥', '≤'), tok))
                    n = len(tok_)
                    j = 0
                    ini = i

                    if debug: print('@DEBUG::A@ tok=%s tok_=%s j=%d n=%d i=%d' % (tok, tok_, j, n, i), file=sys.stderr) #DEBUG
                    while j < n:
                        
                        while source_text[i] in (' ', '\n', '\t'):
                            if debug: print('@DEBUG::B@ j=%d n=%d i=%d' % (j, n, i), file=sys.stderr) #DEBUG
                            i += 1
                            ini = i

                        if source_text[i] in ('َ', 'ً', 'ُ', 'ٌ', 'ِ', 'ٍ', 'ْ', 'ّ'):
                            i+= 1

                        elif tok_[j] == source_text[i]:
                            j += 1 ; i+= 1

                        else:
                            if source_text[i] == 'ـ': #NOTE to deal with "يوماً (٤٥ـ٤٣) وبعضهم" -> يوم+≤ا/NOUN-MS (/PUNC ٤٥٤٣/NUM-MP )/PUNC و+/CONJ
                                i+= 1
                            else:
                                print('FATAL error in %s! i=%d j=%d tok_=%s, tok_[j]=%s source_text[i]=%s' % (fname, i, j, tok_, tok_[j], source_text[i]), file=sys.stderr) #DEBUG
                                sys.exit()
                                
                        if debug: print('@DEBUG::C@ j=%d n=%d i=%d ini=%d source_text[ini:i]=%s' % (j, n, i, ini, source_text[ini:i]), file=sys.stderr) #DEBUG

                    while source_text[i] in ('َ', 'ً', 'ُ', 'ٌ', 'ِ', 'ٍ', 'ْ', 'ّ'):
                        i+= 1
                
                    # get stem
                    if '≥+' in tok: tok = tok.partition('≥+')[-1]
                    if '+≤' in tok: tok = tok.partition('+≤')[0]

                    tok = ''.join(filter(lambda x: x != '+', tok)) # for CONJ, PART and PREP prefixes, and PART, PRON suffixes, and NSUFF

                    tokens.append(Token(source_text[ini:i], tok, REMOVE_DIACRITICS.sub('', source_text[ini:i]), tag, ini, i))
                    if debug: print('@DEBUG@', Token(source_text[ini:i], tok, REMOVE_DIACRITICS.sub('', source_text[ini:i]), tag, ini, i), '\n', file=sys.stderr) #DEBUG
    
        finally:
            os.remove(source_path)
            os.remove(tagged_path)


    if debug:
        print('@DEBUG::doc.text@', repr(doc.text))  #DEBUG
        print('@DEBUG::doc.annotation@', end=' ') ; pprint(doc.annotation)  #DEBUG
        print('@DEBUG::tokens@', end=' ') ; pprint(tokens)  #DEBUG

    newdata = {cfg.get('annotation', 'text key output') : doc.text}
    for label, instances in doc.annotation.items():

        aux = []
        tok_ini = tokens[0].ini  # start at the position of the ini offset of the first token
        tok_end = -1
        for ann in instances:

            ini_key = cfg.get('annotation', 'ini key') if cfg.get('annotation', 'ini key') in ann \
                      else cfg.get('annotation', 'sources ini key')
            
            for i, tok in enumerate(tokens):

                # beginning of annotation instance
                if ann[ini_key] >= tok.ini:
                    tok_ini = i
                    
                # end of annotation instance
                if ann[cfg.get('annotation', 'end key')] > tok.ini:
                    tok_end = i

            if tok_ini == -1 or tok_end == -1:
                print("FATAL ERROR: something went wrong adjusting the offsets in file %s. ini=%d end=%d" %
                      (fname, tok_ini, tok_end), file=sys.stderr)
                sys.exit(1) #DEBUG

            aux.append({cfg.get('annotation', 'value key') : ann.get(cfg.get('annotation', 'value key'), None),
                        cfg.get('annotation', 'ini key') : tok_ini,
                        cfg.get('annotation', 'end key') : tok_end})

            tok_ini = tok_end = -1

        newdata[label] = aux

    try:
        newdata[cfg.get('annotation', 'tokens key')] = list({cfg.get('annotation', 'norm key') : t.var,
                                                             cfg.get('annotation', 'stem key') : t.stem,
                                                             cfg.get('annotation', 'pos key')  : t.tag,
                                                             cfg.get('annotation', 'ini key')  : t.ini,
                                                             cfg.get('annotation', 'end key')  : t.end} for t in tokens)
                        
    except kabkaj.tokeniser.TokenisationError as err:
        print(err, file=sys.stderr)
        sys.exit(1)

    
    #if debug:
    #    print('@DEBUG::newdata@', end=' ') ; pprint(newdata) #DEBUG

    return newdata


if __name__ == '__main__':

    parser = ArgumentParser(description='gather corpus data, tokenise and add metadata to json')
    parser.add_argument('--farasa', action='store_true', help='use farasa tagger')
    option = parser.add_mutually_exclusive_group(required=True)
    option.add_argument('--altafsir_annotated', action='store_true', help='create subcorpus for altafsir annotated')
    option.add_argument('--altafsir_complete', action='store_true', help='create subcorpus for altafsir complete')
    option.add_argument('--hadith_complete', action='store_true', help='create subcorpus for hadith.al-islam complete')
    option.add_argument('--ocred', action='store_true', help='create subcorpus for ocred')
    option.add_argument('--test_altafsir', action='store_true', help='TEST *DEBUG*') #DEBUG
    option.add_argument('--test_hadith', action='store_true', help='TEST *DEBUG*') #DEBUG
    option.add_argument('--all', action='store_true', help='create all subcorpora')
    parser.add_argument('--debug', action='store_true', help='print debug info from tokeniser')
    args = parser.parse_args()
    
    cfg = util.Config.load()

    if args.test_altafsir:
        
        # clean output dir
        print('cleaning %s ...' % cfg.get('test altafsir', 'outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('test altafsir', 'outdir')) if f.is_file()): os.unlink(fpath)

        for fname, doc in getdata.getdata_test_altafsir(cfg):
            print('**', fname, file=sys.stderr) #DEBUG
            
            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_altafsir(fname, cfg, \
                                 *util.loadmeta_altafsir(cfg.get('altafsir', 'madhab meta path'), cfg.get('altafsir', 'tafsir meta path')))
    
            with open(os.path.join(cfg.get('test altafsir', 'outdir'), fname+'.json'), 'w') as outfp:
                json.dump(newdata, outfp, ensure_ascii=False)
        
        sys.exit(0)

    if args.test_hadith:

        # clean output dir
        print('cleaning %s ...' % cfg.get('test hadith', 'outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('test hadith', 'outdir')) if f.is_file()): os.unlink(fpath)

        for fname, fmeta, doc, keytext in getdata.getdata_test_hadith(cfg):

            print('**', fname, keytext, file=sys.stderr) #DEBUG

            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            # add metadata to final data object
            newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_hadith(fname, fmeta, cfg)

            with open(os.path.join(cfg.get('test hadith', 'outdir'),
                     '{base}_{suf}.{ext}'.format(base=fname, suf=keytext, ext='json')), 'w') as outfp:
                    json.dump(newdata, outfp, ensure_ascii=False)
                    

    if args.altafsir_annotated or args.altafsir_complete or args.all:

        madhab_mapping, tafsir_mapping = util.loadmeta_altafsir(cfg.get('altafsir', 'madhab meta path'), cfg.get('altafsir', 'tafsir meta path'))

    if args.altafsir_annotated or args.all:

        # clean output dir
        print('cleaning %s ...' % cfg.get('altafsir', 'annotated outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('altafsir', 'annotated outdir')) if f.is_file()): os.unlink(fpath)

        for fname, doc in getdata.getdata_altafsir_annotated(cfg):
            print('**', fname, file=sys.stderr) #DEBUG
            #print(doc, file=sys.stderr) #DEBUG
            
            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            # add metadata to final data object
            newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_altafsir(fname, cfg, madhab_mapping, tafsir_mapping)
            #pp.pprint(newdata) #DEBUG
    
            with open(os.path.join(cfg.get('altafsir', 'annotated outdir'), fname+'.json'), 'w') as outfp:
                json.dump(newdata, outfp, ensure_ascii=False)

    if args.altafsir_complete or args.all:

        # clean output dir
        print('cleaning %s ...' % cfg.get('altafsir', 'complete outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('altafsir', 'complete outdir')) if f.is_file()): os.unlink(fpath)
        
        for fname, doc in getdata.getdata_altafsir_complete(cfg):
            print('**', fname, file=sys.stderr) #DEBUG
            
            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_altafsir(fname, cfg, madhab_mapping, tafsir_mapping)
    
            with open(os.path.join(cfg.get('altafsir', 'complete outdir'), fname+'.json'), 'w') as outfp:
                json.dump(newdata, outfp, ensure_ascii=False)


    if args.hadith_complete or args.all:

        # clean output dir
        print('cleaning %s ...' % cfg.get('hadith', 'complete outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('hadith', 'complete outdir')) if f.is_file()): os.unlink(fpath)

        for fname, fmeta, doc, keytext in getdata.getdata_hadith_complete(cfg):

            print('**', fname, keytext, file=sys.stderr) #DEBUG

            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            # add metadata to final data object
            newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_hadith(fname, fmeta, cfg)

            with open(os.path.join(cfg.get('hadith', 'complete outdir'),
                     '{base}_{suf}.{ext}'.format(base=fname, suf=keytext, ext='json')), 'w') as outfp:
                    json.dump(newdata, outfp, ensure_ascii=False)

    if args.ocred or args.all:

        # clean output dir
        print('cleaning %s ...' % cfg.get('ocred', 'annotated outdir'), file=sys.stderr) #DEBUG
        for fpath in (f.path for f in os.scandir(cfg.get('ocred', 'annotated outdir')) if f.is_file()): os.unlink(fpath)

        for fname, doc in getdata.getdata_ocred(cfg):

            # --------------------- DEBUG --------------------- #
            #if fname != '98_bin_sauda_altawdi_alqadar': continue
            #print('**', fname, file=sys.stderr)
            # --------------------- DEBUG --------------------- #

            newdata = tokenise_adjust(cfg, doc, fname, args.farasa, args.debug)
    
            try:
                newdata[cfg.get('annotation', 'meta key')] = getmetadata.getmeta_ocred(fname, cfg)
    
                with open(os.path.join(cfg.get('ocred', 'annotated outdir'), fname+'.json'), 'w') as outfp:
                    json.dump(newdata, outfp, ensure_ascii=False)

            except ValueError as e:
                print('{red}{err} Abort conversion for this file.{reset}'.format(err=e, red=RED, reset=RESET), file=sys.stderr)

